/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessages() {
	let firstName = prompt("Enter Your First Name");
	let lastName = prompt("Enter Your Last Name");
	let Age = prompt("Enter Your Age");
	let Location = prompt("Enter Your Location");

	console.log("Hello, " + firstName + " " + lastName + " " + Age + " " + Location + "!");
	console.log("Welcome to Juvix Website");
}

printWelcomeMessages();



    // invoke

/*function printName() {
	console.log("First Name");
	console.log("Last Name");
	console.log("Age");
	console.log("Location");

printName();*/





/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function favoriteBand() {
	console.log("Parokya ni Edgar");
	console.log("All time Low");
	console.log("Mayonaisse");
	console.log("Bamboo");
	console.log("BTS");
	
}

favoriteBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favoriteMovie() {
	console.log("Lion King 99%");
	console.log("Alladin 89%");
	console.log("Frozen 85%");
	console.log("Dragon ball Super 80%");
	console.log("CyberPunk 79%");
	
}

favoriteMovie();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



	alert("Hi! Please add the names of your friends.");

	function showSampleAlert() {
	alert("Hi! Please add the names of your friends.");
}

showSampleAlert();


 function printUsers()
		{

		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 

}
printUsers();



/*console.log(friend1);
console.log(friend2);*/